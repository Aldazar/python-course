## Practice : Loop & Conditions

```
# Install pip dependancy manager
python get-pip.py --user
pip install faker
```

### CliManager v1.0

```
**************************
Welcome to CliManager v1.0
**************************

Select the number of your choice to display screen
[1] - Print my clients
[2] - Add a client
[4] - Delete a client
[5] - Exit
```
#### Generate clients

You can use Faker to generate random clients inside the function `generate_clients`

[Click here for the Faker documentation](https://faker.readthedocs.io/en/master/#basic-usage)

#### Print my clients

```
::::::::::::::::
Print my clients
****************

Select 5 to return to the menu

_______________________________________
| ID | Name | Address | Total spent £ |
---------------------------------------
| ...| .... | ........| ..............|
| ...| .... | ........| ..............|
|____|______|_________|_______________|
```

#### Add a client
 
```
::::::::::::::::
Add a client
****************

Select 5 to return to the menu

Name:
>> John Doe
Address :
>> 15 Doughty Court, Stratford Street, London
Total spent £ :
>> 200000

!! Client added !!
```

#### Edit a client
 
```
::::::::::::::::
Edit a client
****************

Select 5 to return to the menu

Choose an client ID for updating :
>> 10
[Client found]
Let blank if no change
Address [15 Doughty Court, Stratford Street, London] :
>>
Total spent [200000] :
>> 300000

!! Client updated !!
```

#### Delete a client

```
::::::::::::::::
Delete a client
****************

ID of the client to remove :
>> 10
Client to delete not found
>> 1
Client deleted
```

#### Testing

For each feature, you will find related tests to know if all is good with your implementation.

```
python -m unittest tests.py
```
 