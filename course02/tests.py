import unittest
from unittest.mock import patch
from climanager import (
    generate_clients, main,
    display_print_clients,
    display_add_client,
    display_delete_client,
)
from io import StringIO


class CliManagerPrintClientsTests(unittest.TestCase):

    def setUp(self):
        self.out = StringIO()

    def get_output(self):
        return self.out.getvalue().strip()

    def test_generate_clients(self):
        clients = generate_clients()
        self.assertGreater(len(clients), 0)

    @patch('builtins.input', return_value='4')
    def test_exit_if_number_4_selected(self, input):
        main(self.out)
        self.assertEqual("Good Bye !!", self.get_output())

    @patch('builtins.input', return_value='4')
    def test_display_print_client(self, input):
        display_print_clients(self.out)
        self.assertIn("Print my clients", self.get_output())


class CliManagerAddClientTests(unittest.TestCase):

    def setUp(self):
        self.out = StringIO()

    def get_output(self):
        return self.out.getvalue().strip()


    @patch('builtins.input', return_value='4')
    def test_display_add_client(self, input):
        display_add_client(self.out)
        self.assertIn("Add a client", self.get_output())

    @patch('builtins.input', side_effect=iter(['2', '4', '4']))
    def test_display_print_clients_from_menu(self, input):
        #  test successive choices
        # >> 2 = go to add client
        # >> 4 = comeback to menu
        # >> 4 = exit program
        main(self.out)
        self.assertIn("Print my clients", self.get_output())


class CliManagerDeleteClientTests(unittest.TestCase):

    def setUp(self):
        self.out = StringIO()

    def get_output(self):
        return self.out.getvalue().strip()

    @patch('builtins.input', return_value='4')
    def test_display_delete_client(self, input):
        display_delete_client(self.out)
        self.assertIn("Delete a client", self.get_output())

    @patch('builtins.input', side_effect=iter(['3', '4', '4']))
    def test_display_delete_client_from_menu(self, input):
        #  test successive choices
        # >> 3 = go to delete client
        # >> 4 = comeback to menu
        # >> 4 = exit program
        main(self.out)
        self.assertIn("Delete a client", self.get_output())

