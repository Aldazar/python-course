"""
##############################################################################
#
#    CliManager, Open Source Management Solution
#    Copyright (C) 2012-2018 Upidev
#    All Rights Reserved
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
##############################################################################
"""
from faker import Faker
import sys
fake = Faker()

clients = []

choice = 0
is_running = False


def generate_clients():
    clients = [
        {'id': i, 'name': fake.name(), 'address': fake.address()[:13] + "...", 'total_spent': 1000}
        for i in range(10)
    ]

    return clients

def main(out=sys.stdout):
    is_running = True
    clients = generate_clients()

    while is_running:
        choice = display_menu()
        if choice == '4':
            is_running = False
        elif choice == '1':
            display_print_clients(out)

    # Use it to print a message
    out.write("Good Bye !!")


def display_menu():
    print("""
    **************************
    Welcome to CliManager v1.0
    **************************
    Select the number of your choice to display screen
    [1] - Print my clients
    [2] - Add a client
    [3] - Delete a client
    [4] - Exit
    """)

    return input("Choice : ")

def display_print_clients(out=sys.stdout):
    out.write("""
    ::::::::::::::::
    Print my clients
    ****************
    
    Select 4 to return to the menu
    
    ____________________________________________________
    | ID | Name        | Address       | Total spent £ |
    ----------------------------------------------------
    """)

    for client in clients:
        print("    |  {id} | {name} | {address} | {total_spent} |".format(**client))

    return input("Choice : ")


def display_add_client(out=sys.stdout):
    """
    Add a client in the clients list and come back to menu
    """
    pass # Replace pass by your code


def display_delete_client(out=sys.stdout):
    """
    Delete a client in the clients list and come back to menu
    """
    pass # Replace pass by your code

if __name__ == "__main__":
    main()