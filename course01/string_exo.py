# String

def encrypt_password(string):
    # check on google
    # Your code here
    return string

print("#1")
assert encrypt_password("python!44") == "44!nohtyp"
print("OK for string='python!44', password='44!python'")
assert encrypt_password("20&!po18!44") == "44!81op!&02"
print("OK for string='20&!po18!44', password='44!81op!&02'")

#######

def encrypt_password_v2(string):
    # hint : check on google
    # Your code here
    return string

print("#2")
assert encrypt_password_v2("python!44") == "4!otp"
print("OK for string='python!44', password='4!otp'")
assert encrypt_password_v2("20&!po18!44") == "4!1p&2"
print("OK for string='20&!po18!44', password='4!1p&2'")

#######

def encrypt_password_v3(string):
    # hint : sublist and uppercase
    # Your code here
    return string

print("#3")
assert encrypt_password_v3("python!44") == "4!o"
assert encrypt_password_v3("20&!po18!44") == "4!1"