# Cours 01 - Exercices

### Nombres

Calcule le nombre secret en utilisant a,b et ce qui te semble utile.

### Chaine de caractères

Ajoute le code manquant dans la fonction `encrypt_password` pour encrypter le mot de passe
.

Au fur et à mesure de l'exercice, tu peux réutiliser le code créé et l'améliorer pour résoudre les autres versions ( `encrypt_password_v2`, `encrypt_password_v3`)

### Booléen

Ajoute le code manquant à l'intérieur de la fonction

### Jeu de tests

Tu trouveras différents tests pour t'aider à trouver le code manquant.

ex:
```
assert addition(2, 2) == 4
assert addition(2, 3) == 5

# Le code pour cette fonction serait :
def addition(a, b):
    return a + b
```
### Assertion Error
En python, cette exception est levé quand le résulat attendu est différent du résultat retourné.

En exécutant le fichier, si une assertion error est levé, cela signifie que tu dois ajuster le contenu de la fonction.



