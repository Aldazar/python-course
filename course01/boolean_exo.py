# Boolean
print("#1")

def get_or_result(a, b):
    return # Your code here

assert get_or_result(True, True) == True
assert get_or_result(False, False) == False
assert get_or_result(True, False) == True

print("#2")

def get_and_result(a, b):
    return # Your code here

assert get_and_result(False, True) == False
assert get_and_result(True, False) == False
assert get_and_result(True, True) == True

print("#3")

def get_xor_result(a, b):
    return # Your code here

assert get_xor_result(False, True) == True
assert get_xor_result(True, False) == True
assert get_xor_result(True, True) == False




