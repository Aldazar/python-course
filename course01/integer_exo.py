# Integers

def calc_secret_number(a, b):
    # hint : str()
    # Your code here
    return 'result_to_return'

print("#1")
assert calc_secret_number(3, 4) == 34
print("OK for a=3, b=4, secret_number=34")
assert calc_secret_number(2, 1) == 21
print("OK for a=2, b=1, secret_number=21")

#######

def calc_secret_number_2(a, b, c=6):
    # hint : %
    # Your code here
    return 'result_to_return'

print("#2")
assert calc_secret_number_2(10, 20) == 2
print("OK for a=10, b=20, secret_number=2")
assert calc_secret_number_2(15, 10) == -1
print("OK for a=15, b=9, secret_number=3")

#######

def calc_secret_number_3(a, b):
    # hint : - and ()
    # Your code here
    return 'result_to_return'

print("#3")
assert calc_secret_number_3(10, 4) == 6
print("OK for a=10, b=4, secret_number=6")
assert calc_secret_number_3(8, 14) == -6
print("OK for a=8, b=14, secret_number=6")
